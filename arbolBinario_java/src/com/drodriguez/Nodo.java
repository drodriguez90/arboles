package com.drodriguez;

/**
 * @author David Rodríguez Hernández
 * david.rdz.hrz@gmail.com
 * Programa de la creación y recorrido de un árbol binario.
 */
public class Nodo {
     int info;
     Nodo izq;
     Nodo der;
}
