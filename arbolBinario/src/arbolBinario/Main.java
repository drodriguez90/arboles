package arbolBinario;

import java.util.Scanner;

public class Main {
	
	public static void main(String[] args) {
        Arbol abo = new Arbol();
        Scanner input = new Scanner(System.in);
        int opcion = 0;
        do {
            System.out.println("##########");
            System.out.println("Ingrese una opción:");
            System.out.println("1.- Agregar nodo");
            System.out.println("2.- Imprimir arbol");
            System.out.println("3.- Salir");
            System.out.println("##########");
            opcion = input.nextInt();
            
            switch (opcion){
                case 1: 
                    System.out.println("##########");
                    System.out.println("Dato a ingresar:");
                    int data=input.nextInt();
                    abo.insertar(data);
                    break;
                case 2: 
                    System.out.println("########");
                    System.out.println("Árbol");
                    System.out.print("Preorden:");
                    abo.imprimirPre();
                    System.out.print("Inorden:");
                    abo.imprimirEntre();
                    System.out.print("Postorden:");
                    abo.imprimirPost();
                    break;
                default:
                    System.out.print(opcion);
                    System.exit(0);
            }
        } while (opcion<1 || opcion<3 );
       input.close();
    }

}
